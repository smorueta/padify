define(['backbone', 'underscore', 'jquery', 'router', 'js/controllers/MainController'], function (Backbone, _, $, Router, MainController) {
    
    'use strict';

    return function() {
		
		var appRouter;
		appRouter = new Router();
				
		appRouter.on('route:init', function () {
			MainController.trigger('retrieveDocumentsData');
		});

		Backbone.history.start();		
    }();
});