/*globals define, $, console*/

define(['backbone'], function (Backbone) {
    'use strict';
    return Backbone.Router.extend({
        routes: {
            "init": "init"
        }
    });
});