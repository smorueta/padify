/**
 * [create a server. http, express and ecstatic modules node are required.
 * Install npm http, express and ecstatic] in project directory
 */
var http = require('http');
var express = require('express');
var ecstatic = require('ecstatic');

var app = express();

app.use(ecstatic({
    root: __dirname 
}));
http.createServer(app).listen(8000);

console.log('Listening on :8000');