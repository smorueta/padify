# README #

### What is this repository for? ###

* This app is a basic backbone structure built to consume a public JSON API from cloudant. The app shows a list of documents with some data (the publication title, summary and the cover image of the publication) 

* Version 1.0.0

### How do I get set up? ###

The app needs a server node to be executed. It's included in the repository and it has been called "server.js" To run it, open a terminal or windows console and located in the project directory write "node server.js". A instance of server node will be started on port 8000. Then, writing localhost:8000. The initial route is http://localhost:8000/#init

Although every dependency has been included in project, package.json file has been updated to the repository, you can install them directly using "npm install"
  
### Next planned upgrading ###

- Documentation and comments
- Tests (unit tests)
- Build process for generating production-ready code