requirejs.config ({
    baseUrl: '/',
    paths: {
        jquery: "libs/jquery-1.11.3",
        underscore: "libs/underscore-1.8.3",
        backbone: "libs/backbone-1.2.1"
    },
    shim: {
        jquery: {
            exports: '$'
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: 'backbone'
        }
    }
});
// Load the main app module to start the app
requirejs(["main"]);