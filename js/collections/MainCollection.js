define(["backbone","js/models/MainModel", "/js/commands/CommandManager.js"], function (Backbone, MainModel, CommandManager) {
	
	return Backbone.Collection.extend({
		
		model: MainModel,
		
		initialize:function () {
			var commandManager,
				requestDocuments;

			commandManager = new CommandManager();
			requestDocuments = commandManager.execute('requestDocuments', this);			
		},

	    sync: function (method, collection, options) {
	    	options.dataType = "jsonp";
	    	return Backbone.sync(method, collection, options);
	    },
	    
	    parse: function (response) {
        	return response.rows;
    	}

	});
});
