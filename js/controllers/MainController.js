define(['js/collections/MainCollection', '/js/commands/CommandManager.js'], function (MainCollection, CommandManager) {
	var MainController = {};

	_.extend(MainController, Backbone.Events);

	//definition of listeners
	MainController.on('retrieveDocumentsData', function() {
		
		//create collection to save retrieved data
		var collection = new MainCollection();
		
		collection.on('sync', _.bind(function (data) {

			var commandManager,
				requestDocuments;

			commandManager = new CommandManager();
			requestDocuments = commandManager.execute('loadMainView', data);	

		}, this));
	});

	return MainController;
});