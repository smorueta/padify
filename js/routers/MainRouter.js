define(['backbone'], function (Backbone) {
    'use strict';
    var AppRouter = Backbone.Router.extend({
        routes: {
            '/init': 'init'
        }
    }),

    initialize = function () {
        var app_router = new AppRouter();
    };

    return {
        initialize: initialize
    };
});