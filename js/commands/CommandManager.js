define(['underscore', '/js/views/MainManagerView.js'], function (_, MainManagerView) {
	'use strict';

	function CommandManager () {
		
		/**
		 * [autentification and connect data]
		 * @type {Object}
		 */
		var headers,
			url;

		headers = {
			'Authorization' :'Basic asemeacherbsepteditheres:81f0885385265026485780696c1f95f9eb529833aa'
		};

		url = 'http://padify.cloudant.com/test-collection/_all_docs?include_docs=true';
		
		/**
		 * [requestDocuments description]
		 * @param  {[type]} saludo [description]
		 * @return {[type]}        [description]
		 */
		
		this.requestDocuments = function (collection) {
			
			collection.url = url;

			collection.fetch({
				headers: headers,
				success: function(){
					console.log('success fetching data');
				},
				error: function(){
					console.log('error fetching data');
				}
			});
		},

		this.loadMainView = function (collection) {
			var managerView = new MainManagerView({ el: $("#content-js"), collection: collection});

			return managerView;
		}
	};

	/**
	 * [execute description]
	 * @param  {[type]} method [description]
	 * @return {[type]}        [description]
	 */
	CommandManager.prototype.execute = function (method) {
		if (!this[method] || typeof this[method] !== "function") {
			throw "function is required";
		} else {
			this[method].apply(CommandManager, [].slice.call(arguments, 1));
		}
	};

	return CommandManager;
});