define(['backbone', 'underscore'], function (Backbone, _) {
	
	return Backbone.Model.extend({

		parse:function (response, options) {
			var values = response.doc;

			_.map(response.doc, _.bind(function(value, prop){ 

				if (prop.indexOf('title') > -1 || prop.indexOf('summary') > -1) {
					this.set(prop, value);
				} else if (prop.indexOf('cover') > -1) {
					this.set(prop, {});
					this.get(prop).url = value.url;
				}
			}, this));
		}
	});
});