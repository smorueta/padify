
define(['backbone', 'js/views/ListItemView'], function (Backbone, ListItemView) {
	return Backbone.View.extend({

        initialize: function (options) {
            _.bindAll(this, "renderItem");
            
            this.defineRegions();
            this.defineData(options);
            this.render();
        },

        defineRegions:function () {
            this.regions = {
                list: {
                    el: this.$('#list-items-container'),
                    view: ListItemView
                }
            };
        },

        defineData:function (options) {
            this.collection = options.collection;
        },

        renderItem: function (currentRegion, data) {
            if (this.itemView){
                //remove currentView
                this.itemView.remove();
            }

            this.itemView = new this.regions[currentRegion].view({data: data});
            this.regions[currentRegion].el.append(this.itemView.el);
        },

        render: function (selectedModel) {
            if (!selectedModel){
                this.renderItem("list", this.collection);
            } 
        }
	});
});