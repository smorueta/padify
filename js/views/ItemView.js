define(['backbone'], function (Backbone) { 
	return Backbone.View.extend({	

        template: 'js/views/templates/content-list-item-view.html',

        tagName: 'li',

        className: 'item-list',

        initialize:function () {
			(function(view){
				$.get( view.template, function (data) {		
                	template = _.template($(data).html());
                	view.$el.html(template({item:view.model.toJSON()}));  
            	}, 'html')
            })(this);

        }
	});

});