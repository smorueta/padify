define(['backbone', 'js/views/ItemView'], function (Backbone, ItemView) {
	return Backbone.View.extend({	

        tagName: 'ul',

        className: 'list-documents',

        initialize:function (options) {
            this.collection = options.data;

            this.collection.each(function(model){
                this.renderItem.call(this, "list", model);
            }, this);   
        },

        renderItem: function (currentRegion, model) {
            this.itemView = new ItemView({model: model});
            this.$el.append(this.itemView.el);
        }
		
	});

});